#include <algorithm>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <unordered_set>
#include <unordered_map>
#include <stack>
#include <queue>
#include <vector>

enum {
    DFS,
    BFS,
    DP1,
    DN1,
};

using namespace std;

void trim_string(string &s) {
    s.erase(0, s.find_first_not_of(" \n\r\t"));
    s.erase(s.find_last_not_of(" \n\r\t")+1);
}

template <typename Node>
struct graph {
    unordered_set<Node> nodes;
    unordered_map<Node, unordered_set<Node>> edges;

    void add_node(const Node &i) {
	nodes.insert(i);
    }

    void add_edge(const Node &i, const Node &j) {
	edges[i].insert(j);
    }
};

template <typename Node>
void read_makefile(const string& filename, graph<Node>& g, bool directed) {
    ifstream infile(filename);
    string line;
    Node i, j;
    while (getline(infile, line)) {
	size_t split = line.find(':');
	if (split == string::npos) {
	    continue;
	}

	string target  = line.substr(0, split);
	string sources = line.substr(split + 1, line.size() - split);

	trim_string(target);
	trim_string(sources);

	// TODO: Add node
	g.add_node(target);

	stringstream ss(sources);
	string source;
	while (ss >> source) {
	    // TODO: Add edges(s)
	    g.add_edge(target, source);
	    if (!directed)
		g.add_edge(source, target);
	}
    }
}

template <typename Node>
void find_dependencies_dfs(const graph<Node> &g, const Node &target, vector<string> &dependencies) {
    // TODO: Perform graph traversal to find dependencies

  stack<Node> frontier;
  unordered_set<Node> marked;
  frontier.push(target);
  while (frontier.size() > 0) {
    Node u = frontier.top();
    frontier.pop();
	
    if (marked.count(u))
    	continue;

    marked.insert(u);
    auto got = g.edges.find(u);

    if (got != g.edges.end()) {
      for (Node v : got->second) {
	  frontier.push(v);
      }
    }
    
    if ( u != target) {
      dependencies.push_back(u);
    }
  }
}

template <typename Node>
void find_dependencies_bfs(const graph<Node> &g, const Node &target, vector<string> &dependencies) {
    // TODO: Perform graph traversal to find dependencies
  queue<Node> frontier;
  unordered_set<Node> marked;
  frontier.push(target);
  while (frontier.size() > 0) {
    Node u = frontier.front();
    frontier.pop();
	
    if (marked.count(u))
    	continue;

    marked.insert(u);
    auto got = g.edges.find(u);

    if (got != g.edges.end()) {
      for (Node v : got->second) {
	  frontier.push(v);
      }
    }
    
    if ( u != target) {
      dependencies.push_back(u);
    }
  }
}

// TODO: Entry for Dijsktra's Priority Queue
struct Entry {
    int weight;
    string u;
    string v;

    bool operator<(const struct Entry &other) const { return weight > other.weight; }
};

template <typename Node>
void find_dependencies_djk(const graph<Node> &g, const Node &target, vector<string> &dependencies, int weight) {
    // TODO: Perform graph traversal to find dependencies
  priority_queue<Entry> frontier;
  unordered_set<Node> marked;
  frontier.push(Entry{0, target, target});
  while (frontier.size() > 0) {
    auto e = frontier.top();
    frontier.pop();
	
    if (marked.count(e.u))
    	continue;

    marked.insert(e.u);
    auto got = g.edges.find(e.u);

    if (got != g.edges.end()) {
      for (Node v : got->second) {
	  frontier.push(Entry{e.weight + weight,v, e.u});
      }
    }
    
    if ( e.u != target) {
      dependencies.push_back(e.u);
    }
  }
}

int main (int argc, char *argv[]) {
    graph<string> g;
    vector<string> dependencies;

    if (argc != 4) {
    	cerr << "usage: " << argv[0] << " Makefile target" << endl;
    	return 1;
    }

    string path(argv[1]);
    string target(argv[2]);

    read_makefile(path, g, true);

    switch (atoi(argv[3])) {
	case DFS:
	    find_dependencies_dfs(g, target, dependencies);
	    break;
	case BFS:
	    find_dependencies_bfs(g, target, dependencies);
	    break;
	case DP1:
	    find_dependencies_djk(g, target, dependencies, 1);
	    break;
	case DN1:
	    find_dependencies_djk(g, target, dependencies, -1);
	    break;
    }

    //sort(dependencies.begin(), dependencies.end());
    cout << "There are " << dependencies.size() << " dependencies for target " << target << ":" << endl;
    for (auto &dependency : dependencies)
    	cout << '\t' << dependency << endl;

    return 0;
}
