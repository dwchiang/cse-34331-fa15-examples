#include <iostream>
#include <string>
#include <sstream>

using namespace std;

int main(int argc, char *argv[]) {
    string line;

    while (getline(cin, line)) {
    	string key;
    	string value;

    	stringstream ss(line);
    	ss >> key;

    	key = key.substr(0, key.find(":"));

    	while (ss >> value) {
    	    size_t paren = value.find("(");
    	    if (paren == string::npos)
    	    	continue;
	    
	    cout << value.substr(0, paren) << "\t" << key << endl;
	}
    }

    return 0;
}
