#include <iostream>
#include <sstream>

using namespace std;

int main(int argc, char *argv[]) {
    string line;

    while (getline(cin, line)) {
    	string key;
    	string value;

    	stringstream ss(line);
    	ss >> key;

    	while (ss >> value) {
    	    cout << value << "\t" << 1 << endl;
	}
    }

    return 0;
}
