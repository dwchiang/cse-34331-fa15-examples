// search_linear.cpp: simple linear search

#include <cstdlib>
#include <ctime>
#include <algorithm>
#include <iostream>
#include <vector>

const int NITEMS  = 1<<20;
const int NSEARCH = 1<<10;

int main(int argc, char *argv[]) {
    std::vector<int> haystack;

    srand(time(NULL));

    for (int i = 0; i < NITEMS; i++) {
    	haystack.push_back(rand());
    }

    for (int i = 0; i < NSEARCH; i++) {
	int needle = rand();

	std::cout << "Searching for " << needle;
	if (std::find(haystack.begin(), haystack.end(), needle) != haystack.end()) {
	    std::cout << " Yeah!" << std::endl;
	} else {
	    std::cout << " Nope!" << std::endl;
	}
    }

    return 0;
}
