#include <iostream>
#include <memory>

using namespace std;

template <typename T>
struct Node {
    T data;
    struct Node *next;
};

template <typename T>
void print_list(Node<T> *head, const char *prefix) {
    cout << prefix;
    for (Node<T> *curr = head; curr != nullptr; curr = curr->next) {
    	cout << " " << curr->data;
    }
    cout << endl;
}

template <typename T>
Node<T> * reverse_node(Node<T> *curr, Node<T> *prev) {
    Node<T> *tail = curr;

    if (curr->next != nullptr) {
    	tail = reverse_node(curr->next, curr);
    }

    curr->next = prev;
    return tail;
}

template <typename T>
Node<T> * reverse_list(Node<T> *head) {
    return reverse_node(head, static_cast<Node<T> *>(nullptr));
}

int main(int argc, char *argv[]) {
    Node<int> *head = new Node<int>{
    	0, new Node<int>{
    	1, new Node<int>{
    	2, new Node<int>{
    	3, new Node<int>{
    	4, nullptr
    }}}}};

    print_list(head, "Original:");

    head = reverse_list(head);

    print_list(head, "Reversed:");

    return 0;
}
