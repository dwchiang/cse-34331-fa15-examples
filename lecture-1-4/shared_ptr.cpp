#include <iostream>
#include <memory>
#include <string>

using namespace std;

int main(int argc, char *argv[]) {
    shared_ptr<string> s1(new string("Hi"));

    cout << *s1 << ": " << s1->size() << " (" << s1.use_count() << ")" << endl;
   
    shared_ptr<string> s2 = make_shared<string>("Yo");
    cout << *s2 << ": " << s2->size() << " (" << s2.use_count() << ")" << endl;

    shared_ptr<string> s3(s2);
    shared_ptr<string> s4(s2);
    
    cout << *s2 << ": " << s2->size() << " (" << s2.use_count() << ")" << endl;
    cout << *s3 << ": " << s3->size() << " (" << s3.use_count() << ")" << endl;
    cout << *s4 << ": " << s4->size() << " (" << s4.use_count() << ")" << endl;

    return 0;
}
