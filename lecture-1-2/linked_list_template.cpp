// linked_list_template.cpp: Singly Linked List (template)

#include <cstdlib>
#include <iostream>
#include <stdexcept>

const int NITEMS = 10;

// List declaration ------------------------------------------------------------

template <typename T>
class List {
    protected:
        typedef struct Node {   // Why struct instead of class?
            struct Node *next;
            T            data;
        } Node;

        typedef Node * iterator;// What is this?

        Node *head;

    public:
        List() : head(NULL) {}  // Leave out and it segfaults
        iterator front() { return head; }; // Need iterator method b/c internals are protected

        size_t size() const;
        T& at(const size_t i);
        void insert(iterator it, const T &data);
        void push_back(const T &data);
        void erase(iterator it);
};

// List implementation --------------------------------------------------------

template <typename T>
size_t List<T>::size() const {
    size_t size = 0;

    // Condition can also just be node
    for (Node *node = head; node != NULL; node = node->next) {
        size++;
    }

    return size;
}

template <typename T>
T& List<T>::at(const size_t i) {
    Node *node = head;
    size_t   n = 0;

    while (n < i && node != NULL) {
        node = node->next;
        n++;
    }

    if (node != NULL) {
        return node->data;
    } else {
        throw std::out_of_range("invalid list index");
    }
}

// Post-Condition: New Node is created with specified data value and placed
// after the iterator it.
template <typename T>
void List<T>::insert(iterator it, const T& data) {
    // Handle empty list
    if (head == NULL && it == NULL) {
        head = new Node{NULL, data};
        return;
    }

    if (it == NULL) {
        throw std::out_of_range("invalid iterator");
    }

    it->next = new Node{it->next, data};
}

// Post-Condition: New Node is create with specified data value and placed at
// the end of the list.
template <typename T>
void List<T>::push_back(const T& data) {
    // Handle empty list
    if (head == NULL) {
        head = new Node{NULL, data};
        return;
    }

    Node *curr = head;
    Node *tail = head;

    while (curr) {
        tail = curr;
        curr = curr->next;
    }

    tail->next = new Node{NULL, data};
}

template <typename T>
void List<T>::erase(iterator it) {
    if (it == NULL) {
        throw std::out_of_range("invalid iterator");
    }

    if (head == it) {
        head = head->next;
        delete it;
    } else {
        Node *node = head;

        while (node != NULL && node->next != it) {
            node = node->next;
        }

        if (node == NULL) {
            throw std::out_of_range("invalid iterator");
        }

        node->next = it->next;
        delete it;
    }
}

// Main execution -------------------------------------------------------------

int main(int argc, char *argv[]) {
    List<int> list;

    std::cout << "List Size: " << list.size() << std::endl;

    for (int i = 0; i < NITEMS; i++) {
        list.push_back(i);
    }

    std::cout << "List Size: " << list.size() << std::endl;
    std::cout << "List Items:" << std::endl;
    for (size_t i = 0; i < list.size(); i++) {
        std::cout << "List at " << i << " " << list.at(i) << std::endl;
    }

    auto head = list.front();
    list.insert(head, NITEMS + 1);
    list.insert(head, NITEMS + 2);
    list.insert(head->next->next, NITEMS + 3);

    std::cout << "List Size: " << list.size() << std::endl;
    std::cout << "List Items:" << std::endl;
    for (size_t i = 0; i < list.size(); i++) {
        std::cout << "List at " << i << " " << list.at(i) << std::endl;
    }

    list.erase(list.front());
    list.erase(list.front()->next);
    std::cout << "List Size: " << list.size() << std::endl;
    std::cout << "List Items:" << std::endl;
    for (size_t i = 0; i < list.size(); i++) {
        std::cout << "List at " << i << " " << list.at(i) << std::endl;
    }

    return 0;
}
