#include <iostream>
#include <map>
#include <set>
#include <string>
#include <sstream>
#include <vector>

#include <boost/algorithm/string/trim.hpp>

using namespace std;

typedef map<string, vector<string>> Graph;

void load_graph(Graph &g) {
    string line;

    // Clear Graph
    g.clear();

    // Read rules
    while (getline(cin, line)) {
    	size_t split = line.find(':');
    	if (split == string::npos) {
    	    continue;
	}

	string target  = line.substr(0, split);
	string sources = line.substr(split + 1, line.size() - split);

	boost::trim(target);
	boost::trim(sources);

	if (g.find(target) == g.end()) {
	    g[target] = vector<string>();
	}

	stringstream ss(sources);
	string source;
	while (ss >> source) {
	    g[target].push_back(source);
	}
    }
}

void dump_graph(Graph &g) {
    cout << "digraph tgf {" << endl;

    for (auto &pair : g) {
    	auto target  = pair.first;
    	auto sources = pair.second;
	for (auto &source : sources) {
	    cout << "\t\"" << target << "\" -> \"" << source << "\";" << endl;
	}
    }

    cout << "}" << endl;
}

int main(int argc, char *argv[]) {
    Graph g;

    load_graph(g);
    dump_graph(g);

    return 0;
};
