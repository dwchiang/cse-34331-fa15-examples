#include <bitset>
#include <random>

using namespace std;

#define NUMBERS 1<<25

int main(int argc, char *argv[]) {
    bitset<NUMBERS> s;
    default_random_engine e;
    uniform_int_distribution<uint32_t> d;

    for (size_t i = 0; i < NUMBERS; i++) {
    	s.set(d(e) % NUMBERS);
    }
    
    for (size_t i = 0; i < NUMBERS; i++) {
    	s.test(i);
    }

    return 0;
}
